"""
This file provides the provisional results of the example we find in Briggs chapter five. 
"""

import polynom as pol

# Briggs Chapter 5
n =  45113

# Briggs Chapter 5.1
poly = pol.Polynom([8, 29, 15, 1])
d = 3
m = 31

# Briggs Chapter 5.2
rfb = [(1, 2),
       (1, 3),
       (1, 5),
       (3, 7),
       (9, 11),
       (5, 13),
       (14, 17),
       (12, 19),
       (8, 23),
       (2, 29),
]

# Briggs Chapter 5.3
afb = [(0, 2),
       (6, 7),
       (13, 17),
       (11, 23),
       (26, 29),
       (18, 31),
       (19, 41),
       (13, 43),
       (1, 53),
       (46, 61),
       (44, 67),
       (2, 67),
       (6, 67),
       (50, 73),
       (47, 79),
       (73, 79),
       (23, 79),
       (28, 89),
       (62, 89),
       (73, 89),
       (28, 97),
       (87, 101),
       (47, 103),
]

# Briggs Chapter 5.4
# choosing an quadratic character base
qcb_upper_bound = 110
qcb = [(80, 107),
       (8, 107),
       (4, 107),
       (52, 109),
       (99, 109),
]
number_of_smoothies_necessary = 39

# Briggs Chapter 5.5
smoothies = [(-73,1),
             (-2,1),
             (-1,1),
             (2,1),
             (3,1),
             (4,1),
             (8,1),
             (13,1),
             (14,1),
             (15,1),
             (32,1),
             (56,1),
             (61,1),
             (104,1),
             (116,1),
             (-5,2),
             (3,2),
             (25,2),
             (33,2),
             (-8,3),
             (2,3),
             (17,3),
             (19,4),
             (48,5),
             (54,5),
             (313,5),
             (-43,6),
             (-8,7),
             (11,7),
             (38,7),
             (44,9),
             (4,11),
             (119,11),
             (856,11),
             (536,15),
             (5,17),
             (5,31),
             (9,32),
             (-202,43),
             (24,55),
]

# Briggs Chapter 5.6

# Briggs Chapter 5.7

dependend_smoothie_list = [[
        (-1,1),
        (3,1),
        (13,1),
        (104,1),
        (3,2),
        (25,2),
        (-8,3),
        (48,5),
        (54,5),
        (-43,6),
        (-8,7),
        (11,7),
        (856,11),
]]


# Briggs Chapter 5.8
delta = pol.Polynom({0 : 24765692886531904,
                     1 : 15388377355799440,
                     2 : 2051543129764485,
})
beta = pol.Polynom({0 : 3889976768,
                    1 : 3686043120,
                    2 : 599923511,
})

# Briggs Chapter 5.9
field_primes_needed = 3
field_primes_lower_bound = 9850
field_primes = [9851,
                9907,
                9929,
]
remainder_dict = {9851: 5694,
                  9907: 4152,
                  9929: 2002,
}

# Briggs Chapter 5.11
x = 694683807559

# Briggs Chapter 5.12
y = 3824 * 31746503388600

factor_1 = 197
factor_2 = 229
