import dixon
import trying
import quadratic_sieve as qsieve
import gnfs
import common as co

def non_trivial_factor(n, method="dixon"):
    """
    Returns a non trivial factor of n, None if n is prime
    """ 
    if method == "trying":
        return trying.non_trivial_factor(n)
    elif method == "dixon":
        return dixon.non_trivial_factor(n)
    elif method == "quadratic_sieve":
        return qsieve.non_trivial_factor(n)
    elif method == "gnfs":
        return gnfs.gnfs(n)
    else:
        raise ValueError ("unknown strategie of factoring: {}".format(method))

def complete_factor_list(n, method="quadratic_sieve"):
    """
    returns a list of all prime factors of n
    """
    if method == "dixon":
        # dixons method is not good for this
        raise NotImplementedError()
    if method == "quadratic sieve":
        pass
    if method == "trying":
        pass

    factor1 = non_trivial_factor(n, method)
    if factor1 is None:
        # seems like n is a prime
        return [n]
    factor2 = n // factor1

    factorlist_1 = complete_factor_list(factor1, method)
    factorlist_2 = complete_factor_list(factor2, method)
    return sorted(factorlist_1 + factorlist_2)


