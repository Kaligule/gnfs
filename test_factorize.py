import unittest
import hypothesis.strategies as st
from hypothesis import given, note, assume, example
import exceptions as ex
import hypothesis_strategies as hyst
import factorize
import trying


class TestFactorize:
    @given(hyst.primes(),
           hyst.primes(),
           hyst.fac_strategies
    )
    def test_rsa_number_has_factor(self, prime1, prime2, strategy):
        assume(prime1 != prime2)
        rsa_number = prime1 * prime2
        factor = factorize.non_trivial_factor(rsa_number, strategy)
        if factor is None:
            note("No factor of {} x {} = {} found".format(prime1, prime2, rsa_number))
            raise ex.NoFactorFound(rsa_number)
        else:
            assert factor == prime1 or factor == prime2

    @given(hyst.primes(),
           hyst.primes(),
    )
    def test_unique_factor_list_trying(self, prime1, prime2):
        assume(prime1 != prime2)
        factorlist = trying.unique_factor_list(prime1 * prime2)
        assert factorlist.sort() == [prime1, prime2].sort()

    @given(st.integers(min_value=6, max_value=20))
    def test_primefactors_mutliply(self, n):
        # whenmultiplying the primefactors of a number n, we expect to get the number back
        factorlist = factorize.complete_factor_list(n)
        product = 1
        for factor in factorlist:
            product *= factor
        assert product == n

