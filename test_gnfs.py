import logging
import common as co
import exceptions as ex
import gnfs

co.setup_logger()
log = logging.getLogger('test_gnfs_logger')

# Not tested yet
# n = 2**(2**5) + 1

# Seems to work

problem_list =[
    (13, 17),
    (197, 229),
    (2013, 2011),
]

quickly = False
solutions_found=dict()
if quickly:
    try:
        n = 877 * 977
        factor = gnfs.gnfs(n)
    except ex.StrategyFailed:
        log.info("We didn't find a factor, but at least we didn't have an error.")
    else:
        assert n % factor == 0
else:
    for factors in problem_list:
        p, q = factors
        n = p * q
        try:
            factor = gnfs.gnfs(n)
        except ex.StrategyFailed:
            solutions_found[n] = None
            log.info("No factor found of {}={}*{}".format(n,p,q))
        else:
            assert n % factor == 0
            solutions_found[n] = factor
            log.info("Factor {} found of: {}={}*{}".format(factor,n,p,q))
        log.info("These are the results we had:")
        log.info(solutions_found)
