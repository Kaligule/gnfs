from math import ceil, floor, log
import common as co
import logging

# logger
co.setup_logger()
log = logging.getLogger('sieve_logger')

def gnfs_sieve(r_factor_base, a_factor_base, number_of_smoothies_needed, poly, m, method = "exact"):
    """Find smoothies for gnfs."""
    # If something in this algorythm should be parallelized, it should
    # be this part
    smoothies = []
    b = 1
    while len(smoothies) < number_of_smoothies_needed:
        new_smoothies = gnfs_sieve_specific_b(r_factor_base, a_factor_base, poly, m, b, method)
        smoothies += new_smoothies
        b += 1
    return smoothies

def gnfs_sieve_specific_b(r_factor_base, a_factor_base, poly, m, b, method):
    # Interesting effects have been seen for u in [1000, 10000, 50000]
    u = 1000
    null = u
    log.debug("Start sieving for b = {}".format(b))
    if method in ["exact", "single-division"]:
        r_sieve_array = [a+b*m  for a in range(-u, u+1)]
        a_sieve_array = [co.norm(a, b, poly) for a in range(-u, u+1)]
    elif method == "logarithmic":
        r_sieve_array = [0 for a in range(-u, u+1)]
        a_sieve_array = [0 for a in range(-u, u+1)]

    # Sieve rational
    for (r,p) in r_factor_base:
        log.debug("Rational sieving the for (r,p) = {}".format((r,p)))
        r_sieve_array = sieving_step(r_sieve_array, (r,p), b, method)
    log.debug("Rational sievieng done for b = {}".format(b))

    # Sieve algebraic
    for (r,p) in a_factor_base:
        log.debug("Algebraic sieving the for (r,p) = {}".format((r,p)))
        a_sieve_array = sieving_step(a_sieve_array, (r,p), b, method)
    log.debug("Algebraic sievieng done for b = {}".format(b))

    # Harvesting
    potential_smoothies = []
    for a in range(-u, u+1):
        rational_rest = r_sieve_array[null+a]
        algebraic_rest = a_sieve_array[null+a]
        if method in ["exact", "single-division"]:
            if abs(rational_rest) == 1 and abs(algebraic_rest) == 1:
                log.debug("This might be a smoothie.")
                potential_smoothies.append((a, b))
        elif method == "logarithmic":
            if (a+b*m == 0 or rational_rest >= math.log(abs(a+b*m))) and \
               algebraic_rest >= math.log(abs(co.norm(a, b, poly))):
                log.debug("This might be a smoothie.")
                potential_smoothies.append((a, b))
    log.debug("Potential smoothies for b = {}: {}".format(b, potential_smoothies))

    # Now we have to double-check whether our potential smoothies are
    # sioothies (this is not really necessary if the sieving is done
    # exact. But there is no harm in doublechecking. The number of
    # smoothies is not too high normaly.
    smoothies = []
    for a, b in potential_smoothies:
        if not co.is_smooth(a+b*m, r_factor_base):
            log.debug("{} looked like a smoothie, was not smooth rationally.")
        elif not co.is_smooth((a, b), a_factor_base, m, poly):
            log.debug("{} looked like a smoothie, was not smooth algebraically.")
        else:
            smoothies.append((a,b))
    log.info("Smoothies for b = {}: {}".format(b, smoothies))

    return smoothies

def sieving_step(sieve_array, prime, b, method):
    r, p = prime # doesn't matter wheter algebraic or rational
    u = (len(sieve_array) - 1) // 2
    null = u

    # calculating range for k
    k_min = int(ceil ((-u + b*r) / p))
    k_max = int(floor(( u + b*r) / p))
    log.debug("k will be in range({},{})".format(k_min, k_max))
    log.debug("a will be at least {} and at most {}".format(p*k_min-b*r, p*(k_max-1)-b*r))

    for k in range(k_min, k_max+1):
        a = p*k - b*r
        if method == "exact":
            sieve_array[null + a], _ = co.divide_out(sieve_array[null + a], p)
        elif method == "single-division":
            assert sieve_array[null + a] % p == 0
            sieve_array[null + a] //= p
        elif method == "logarithmic":
            sieve_array[null + a] += math.log(p)
    return sieve_array
