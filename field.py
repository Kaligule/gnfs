import polynom as pol
import logging
import common as co

co.setup_logger()
log = logging.getLogger("field_logger")
log.debug("field_logger is up and running")

class Field():
    """A finite field is represented by """
    def __init__(self, field_characteristic, splitting_polynom=None):
        self.characteristic = field_characteristic
        if splitting_polynom is None:
            self.splitting_polynom = self.construct_irreducible_polynom(degree=2)
        elif isinstance(splitting_polynom, pol.Polynom):
            self.splitting_polynom = splitting_polynom
        elif isinstance(splitting_polynom, int):
            # it is meant that the splitting polynom should have the given degree
            self.splitting_polynom = self.construct_irreducible_polynom(degree=splitting_polynom)

    def __str__(self):
        d = self.splitting_polynom.degree()
        if d == 1:
            return "F_{}".format(self.characteristic)
        else:
            return "F_({}^{})".format(self.characteristic, d)

    def __repr__(self):
        return self.__str__()

    def quadratic_non_residue(self):
        """Find a quadratic non-residue in the finite field defined by
        mod_polynom and field_characteristic.

        This is done by brute force, but since half of the elements of
        the field should be non-residue, thats just fine. We will only
        find polynoms of degree 2 for now.
        """
        for linear_part in range(1, self.characteristic):
            for constant_part in range(self.characteristic):
                p = pol.Polynom({0:constant_part, 1:linear_part})
                if not co.legendre_symbol(p, self):
                    return p
        raise Exception("Didn't find a quadratic non-risidue.")

    def construct_irreducible_polynom(self, degree = 2):
        """construct a monic polynom of degree 2, which is irreducible
        over Z/Z*field_characteristic
        """
        p = pol.Polynom({degree:1})
        while not p.is_irreducible_over_F_p(self.characteristic):
            add_at = 0
            while add_at is not None:
                current_coefficient = p[add_at]
                new_coefficient = (current_coefficient + 1) % self.characteristic
                p[add_at] = new_coefficient
                if new_coefficient == 0:
                    add_at += 1
                    if add_at == degree:
                        raise Exception("Didn't find an irreducible monic polynom of degree {} over Z/Z{}".format(degree, self.characteristic))

                else:
                    add_at = None
        return p

    def __abs__(self):
        """The number of elements in the field"""
        return self.characteristic ** self.splitting_polynom.degree()

    def norm(self, element):
        """See Briggs chapter 4.7"""
        exponent = (abs(self)-1) // (self.characteristic-1)
        norm = pow(element, exponent, self)
        if norm.constant():
            return norm.constant_part()
        else:
            raise Exception
