#!/usr/bin/python3
"""Factorize numbers with quadratic sieve"""

import numpy as np
import primelist
import basic_algebra as ba
import common as co
import tonelli_shanks as ts
import logging

def non_trivial_factor(n):
    """
    Find a non_trivial_factor of n by using quadratic sieve method.
    https://en.wikipedia.org/wiki/Quadratic_sieve
    """

    # logger
    co.setup_logger()
    log = logging.getLogger('quadratic_sieve_logger')

    log.info("Trying to find a nontrivial factor of n = {}".format(n))

    # We use a factorbasis with upper bound smoothness_bound, which is
    # just guessed until now. There might be a more clever way to
    # choose a factor basis.

    # We only need primes p so that n is a quadratic residuum modulo
    # p. This is exactly what the legendre_symbol tells us (I wonder
    # if all this stuff was invented exactly for this purpose)

    # There /is/ a forular for the value of the smoothness bound which
    # been found here:
    # https://de.wikipedia.org/wiki/Quadratisches_Sieb
    # smoothness_bound = int(np.sqrt(np.exp(np.sqrt((np.log(n))*np.log(np.log(n)))))) + 5
    # It seems that there is black magic behind this, though. It
    # wasn't always optimal im my experiments, too.

    smoothness_bound = 100
    log.info("The factorbasis will be the set of primes not higher then {}.".format(smoothness_bound))

    factorbasis = [p for p in primelist.primelist if p <= smoothness_bound and co.legendre_symbol(n, p, True) == 1]
    log.info("The choosen factorbasis has length {} ".format(len(factorbasis)))
    log.info("The choosen factorbasis is {} ".format(factorbasis))

    # A smoothie is a number x, for wich f(x) is smooth relative to
    # the factorbasis we have choosen. f(x)=x^2 - n in the quadratic sieve algorythm.

    f = lambda x: x**2 - n

    # list of the smoothies found
    smoothies = []

    # finding smoothies by sieving

    # create an array which contains a big amount of possible smoothies, then map f on that array
    smoothie_min = 0
    smoothie_max = 10000
    smoothie_array = range(smoothie_min, smoothie_max+1)
    smooth_number_array = np.array([f(x) for x in smoothie_array], dtype=object)

    for prime in factorbasis:
        # We know root1 exists since we restricted our factorbasis to
        # those primes that create a field where n is a prime.
        root1 = ts.modular_sqrt(n, prime)
        root2 = prime - root1
        for root in [root1, root2]:
            # We have to go in both directions
            for index in range(((0-smoothie_min)+root) % prime, smoothie_max + 1, prime):
                smooth_number_array[index] = co.divide_out(smooth_number_array[index], prime)[0]
    for index in range((smoothie_max - smoothie_min)+1):
        if smooth_number_array[index] == 1:
            smoothie = smoothie_array[index]
            assert co.is_smooth(f(smoothie), factorbasis)
            smoothies.append(smoothie)

    log.info("Smoothies found: {}".format(smoothies))

    # Construct a list of dependencies. A dependency is a list of
    # smoothies that are dependend. Every dependency means that a
    # Difference of squares can be constructed (and so, possibly, a
    # factor of n can be found).
    dependend_smoothie_list = co.dependend_smoothies(smoothies, factorbasis, f,)
    for dependend_smoothies in dependend_smoothie_list:
        log.debug("These smoothies are dependend: {}".format(dependend_smoothies))
    log.info("Number of sets of depended smoothies we found: {}".format(len(dependend_smoothie_list)))

    for dependend_smoothies in reversed(dependend_smoothie_list):
        log.info("Trying to find a factor with this set of smoothies: {}".format(dependend_smoothies))
        x        = ba.product([ smoothie    for smoothie in dependend_smoothies ])
        y_square = ba.product([ f(smoothie) for smoothie in dependend_smoothies ])
        y = ba.isqrt(y_square)

        # We now have a difference of squares
        log.info("    x = {}, y = {}".format(x, y))
        log.info("    y^2 = {} = {} = x^2 (mod {})".format( pow(x, 2), pow(y, 2), n,))

        # With any luck, we can compute a divisor from x and y now.
        maybe_factor = co.difference_of_squares(n, root1=x, root2=y)
        if maybe_factor:
            log.info("Factor found: {}".format(maybe_factor))
            return maybe_factor
        # This kernel_vector didn't work out. We will just try the
        # next one.
        log.info("    This attempt failed.")

    log.info("No factor found.")
    return None
