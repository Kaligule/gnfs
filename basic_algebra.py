import polynom as pol
import logging
from functools import reduce
from operator import mul
import common as co

co.setup_logger()
log = logging.getLogger("basic_algebra_logger")

def isqrt(n):
    """
    Find an positive integer x that satisfies x*x == n if it exists,
    throw an error if it doesn't.
    """
    x = n
    y = (n+1)//2
    while y < x:
        x = y
        y = (x + n // x) // 2
    if x ** 2 != n:
        raise ValueError("Input was not a perfect square")
    else:
        return x

def convert_to_base(n, base):
    """Convert number n from base 10 to base base, it is returned as a list (lower exponents first).
    """
    assert n >= 0, "base has to be a positive integer"
    assert base > 1, "base has to be an integer bigger then 1"
    if n == 0:
        return [0]
    digits = []
    while n:
        digits.append(int(n % base))
        n //= base
    return digits

def chinese_remainder_theorem(dictionary, modulo = None):
    """We take a dictionary of the form {prime1: remainder1, prime2:
    remainder2 ...} and eventually an integer modulo (If we only need
    the result % modulo and want to avoid big numbers).  We assume
    that prime1, prime2 ... are primes (it might be enough for them to
    be coprime).

    We try to find a number result that satisfies:
    result % prime1 == remainder1
    result % prime2 == remainder2
    ...
    """
    # log.debug("chinese_remainder_theorem was called with ({}, {})".format(dictionary, modulo))
    product_of_all_primes = reduce(mul, dictionary.keys(), 1)
    result_summands = {}
    for prime, remainder in dictionary.items():
        dictionary[prime] = remainder % prime
    # log.debug("Product of all primes is {}.".format(product_of_all_primes))

    if modulo is None:
        for prime, remainder in dictionary.items():
            product_of_all_primes_but_prime = product_of_all_primes // prime
            inverse_of_all_but_prime = pol.inverse_in_finite_field(product_of_all_primes_but_prime, prime)
            result_summands[prime] = inverse_of_all_but_prime * remainder * product_of_all_primes_but_prime
            # log.debug("For prime {} we will add {} to the result.".format(prime, result_summands[prime]))
        # log.debug("The primes and the summands they give us: {}.".format(result_summands.values()))
        result = sum(result_summands.values()) % product_of_all_primes
    else:
        unrounded_r = {}
        for prime, remainder in dictionary.items():
            product_of_all_primes_but_prime = product_of_all_primes // prime
            inverse_of_all_but_prime = pol.inverse_in_finite_field(product_of_all_primes_but_prime, prime)
            unrounded_r[prime] = (inverse_of_all_but_prime * remainder) / prime
            result_summands[prime] = (inverse_of_all_but_prime * remainder * product_of_all_primes_but_prime) % modulo
        # technically we should ad 0.5 to unrounded_r before rounding
        # Briggs doesn't do that for some reason.
        r = int(sum(unrounded_r.values()) + 4.999)
        log.debug("Rounded r from {} to {}.".format(sum(unrounded_r.values()), r))
        result_summands["r"] = -((r * product_of_all_primes) % modulo)
        log.debug("We will substract (r*P)%n=({}*{})%{}={} from result.".format(r, product_of_all_primes, modulo, -result_summands["r"]))
        log.debug("The primes and the summands they give us: {}.".format(result_summands.values()))
        result = sum(result_summands.values()) % modulo
    # log.debug("Result: {}".format(result))
    return result

def product(list):
    """
    Stupid and not even fast function to multiply elements of a
    list. Python has a built in function for everthing but this.

    Optimize if you want, this is good enough for now.
    """
    result = 1
    for faktor in list:
        result *= faktor
    return result
