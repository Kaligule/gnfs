"""strategies for hypothesis"""

import hypothesis.strategies as st
import primelist
import field
import polynom

def primes(min_value = 2, max_value = 9973):
    if (min_value, max_value) != (2,9973):
        possible_primes = [prime for prime in primelist.primelist if min_value <= prime <= max_value]
    else:
        possible_primes = primelist.primelist
    return st.integers(min_value=2, max_value=len(possible_primes)-1).map(lambda i: possible_primes[i])
fields = st.tuples(primes(max_value=15), st.just(2)).map(lambda params: field.Field(params[0],params[1]))
fac_strategies = st.one_of(st.just("dixon"), st.just("quadratic_sieve"), st.just("trying"))
def polynoms(min_degree = -1, max_degree = 10, trivial_allowed = True):
    return st.lists(elements = st.integers(), min_size = min_degree + 1, max_size = max_degree + 1).map(lambda coeffs: polynom.Polynom(coeffs)).filter(lambda p: trivial_allowed or not p.trivial()).filter(lambda p: min_degree <= p.degree())
