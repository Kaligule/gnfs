#!/usr/bin/python3
"""Factorize numbers with dixons algorythm"""

import fractions as frac
import numpy as np
import primelist
import common as co
import logging.config
import basic_algebra as ba

def non_trivial_factor(n):
    """
    Find a non_trivial_factor of n by using dixons method.
    https://en.wikipedia.org/wiki/Dixon%27s_factorization_method
    """

    # logger
    co.setup_logger()
    log = logging.getLogger('dixon_logger')

    log.info("Trying to find a nontrivial factor of n = {}".format(n))

    # We use a factorbasis with upper bound b, which is calculated by
    # some blackmagic formular (found in the wikipedia article
    # above). I find it's values are pretty high, but at least it
    # never was so low we didn't find enough smooth numbers.

    # This would give us an error when calculating the smoothness_bound
    if n == 2 or n == 3:
        return None
    smoothness_bound = int(np.exp(np.sqrt(np.log(n) * np.log(np.log(n)))))
    log.info("The factorbasis will be the set of primes not higher then {}.".format(smoothness_bound))

    factorbasis = [p for p in primelist.primelist if p <= smoothness_bound]
    log.info("The choosen factorbasis has length {} ".format(len(factorbasis)))
    log.info("The choosen factorbasis is {} ".format(factorbasis))

    # A smoothie is a number x, for wich f(x) is smooth relative to
    # the factorbasis we have choosen. f(x)=x^2 % n in dixons algorythm.

    f = lambda x: pow(x,2,n)

    # TODO We will need a big number of smoothies. How many? Not
    # clear. Wikipedia states that "it's generally sufficient that the
    # number of relations be a few more than the size of P", but how
    # few? Lets just choose a number
    smoothie_total = len(factorbasis) + 1

    # list of the smoothies found
    smoothies = []

    # finding smoothies by try and error

    # A smoothie r < sqrt(n) won't help us, cause in this case holds:
    # r**2 % n == r**2, and iff r**2 is smooth, so is r itself.
    maybe_smoothie = int(np.sqrt(n))
    while len(smoothies) < smoothie_total:
        # We are using consecutive values for potential smoothies
        # here, but you could as well use random integers.
        maybe_smoothie += 1

        maybe_smooth_number = maybe_smoothie**2  % n
        factordict = co.is_smooth(maybe_smooth_number, factorbasis)
        if factordict is not None:

            smoothies.append(maybe_smoothie)
            # log.debug("Smoothie found: {}".format(maybe_smoothie))

    log.info("Smoothies found: {}".format(smoothies))

    # Construct a list of dependencies. A dependency is a list of
    # smoothies that are dependend. Every dependency means that a
    # Difference of squares can be constructed (and so, possibly, a
    # factor of n can be found).
    dependend_smoothie_list = co.dependend_smoothies(smoothies, factorbasis, f,)
    for dependend_smoothies in reversed(dependend_smoothie_list):
        log.debug("These smoothies are dependend: {}".format(dependend_smoothies))
    log.info("Number of sets of depended smoothies we found: {}".format(len(dependend_smoothie_list)))

    for dependend_smoothies in dependend_smoothie_list[::-1]:
        log.info("Trying to find a factor with this set of smoothies: {}".format(dependend_smoothies))
        x        = ba.product([ smoothie    for smoothie in dependend_smoothies ])
        y_square = ba.product([ f(smoothie) for smoothie in dependend_smoothies ])
        y = ba.isqrt(y_square)

        # We now have a difference of squares
        log.info("    x = {}, y = {}".format(x, y))
        log.info("    y^2 = {} = {} = x^2 (mod {})".format(pow(x, 2), pow(y, 2), n,))

        # With any luck, we can compute a divisor from x and y now.
        maybe_factor = co.difference_of_squares(n, root1=x, root2=y)
        if maybe_factor:
            log.info("Factor found: {}".format(maybe_factor))
            return maybe_factor
        # This kernel_vector didn't work out. We will just try the
        # next one.
        log.info("    This attempt failed.")

    log.info("No factor found.")
    return None
