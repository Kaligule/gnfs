import logging
import polynom as pol
import exceptions as ex
import basic_algebra as ba

# Given an integer to factorize, one of the first steps of GNFS is to
# choose a monic, irreducible (TODO over what?) polynom p of degree d
# with a root m (mod n).

# This is a step where much can be won by choosing wisely.


def choose_parameters(n, strategy, degree_must_be_odd = False):
    log = logging.getLogger("choose_parameters_logger")
    log.debug("Choose strategy was called with {}, {}, {}".format(n, strategy, degree_must_be_odd))
    # a dict which maps strategy names to functions to choose the polynome
    strategy_dict = {
        "base-m" : choose_polynome_base_method,
        "base-m-reduced" : choose_polynome_base_method_reduced,
        }
    choose_polynome = strategy_dict[strategy]
    try:
        d = choose_degree(n, degree_must_be_odd)
        (p,m) = choose_polynome(n, d)
    except ex.StrategyFailed:
        # This is bad, our strategy failed.
        # We might choose another strategy here
        raise ex.StrategyFailed()
    # TODO: assert that p is irreducible. If p is reducible, the we
    # can find polynoms g,h so that p = g*h. This means that
    # n=p(m)=g(m)*h(m) and we would have found a factor of n without
    # too much work.
    log.info("Choosing Polynom {} (of degree {}) and root {}".format(p,d,m))
    return(p,d,m)

def choose_degree(n, degree_must_be_odd = False):
    """Found these numbers in 4.1 of Briggs"""
    if n < 10**50:
        # You shouldn use the GNFS for such small numbers, but if you
        # think you have to, just use d = 3
        d = 3
    elif 10**50 <= n < 10**80:
        d = 3
    elif 10**80 <= n < 10 ** 110:
        d = 4
    else:
        d = 5

    if degree_must_be_odd and d % 2 == 0:
        return d + 1
    else:
        return d

def choose_polynome_base_method(n, d):
    m = int(pow(n, 1.0 / d))
    # assert that m is not already a factor of n
    # This would lead to a polynom of the form p(x)=m^d
    # which is most certainly not irreducible
    # Also, we were already done then.
    if n % m == 0:
        raise ex.FactorFoundEarly(m)
    elif m ** (d+1) <= n:
        # The polynome will have a higer degree then d
        raise ex.StrategyFailed()
    elif 2 * m ** d <= n:
        # The polynome will not be monic
        raise ex.StrategyFailed()
    coef_list = ba.convert_to_base(n, m)
    p = pol.Polynom(coef_list)
    return(p,m)

def choose_polynome_base_method_reduced(n, d):
    """This comes directly from Briggs Chapter 6.1. We generate a polynom
    poly by the base-m method and then try to make the absolute value
    of the coefficients smaller.

    By the construction of poly we can assume that every coefficient c
    holds 0 <= c < m. We can shift things around a little bit so that
    -m/2 < c <= m/2 (so abs(c) <= m/2) and still preserve its degree d
    and the 2 properties we demanded from it, namely:

    * poly(m) % n == 0
    * poly[d] == 1

    The only coefficient we can not make smaller without loosing the
    second property is poly[d-1].
    """

    poly, m = choose_polynome_base_method(n,d)
    for exponent in range(0, d-1):
        coefficient = poly[exponent]
        assert -m < coefficient <= m
        if coefficient > m/2:
            poly[exponent] -= m
            poly[exponent+1] += 1
    return poly, m

def choose_factorbase_sizes(n, poly, m):
    """Decide how big the factorbases should be. This is a black art and
since we are not experienced at it, we just provide some default
values."""
    rfb_size = 10
    afb_size = 23
    qcb_size = 10
    return  rfb_size, afb_size, qcb_size

