import hypothesis.strategies as st
from hypothesis import given, note, assume, example
import hypothesis_strategies as hyst
import field as f
import polynom as pol
import common as co

class TestField:
    @given(hyst.fields,
          )
    def test_quadratic_non_residue(self, field):
        p = field.quadratic_non_residue()
        assert not co.legendre_symbol(p, field)


