import logging
import fractions as frac
import trying
import common as co
import field as f
import basic_algebra as ba

co.setup_logger()
log = logging.getLogger("polynom_logger")

class Polynom(dict):
    """
    polynom class, meant for polynoms with integer coefficients. It
    might work with fractions, floats or even complex numbers, but
    this was never tested.
    """

    def __init__(self, coefficients=None, roots=None):
        # A polynom is a dictionary, containing the nonzero
        # coefficients
        #
        # For example:
        # f = Polynom({3:4, 2:3, 1:0, 0:-1})
        # f = -1 + 3*x^2 + 4*x^3

        # Polynom given by a list of roots
        if isinstance(roots, list) and coefficients is None:
            pol = Polynom([1])
            for root in roots:
                # multiply pol by 1-root
                pol *= Polynom({0: -root, 1:1})
            dict.__init__(self, pol)
        # Polynom given by a list of coefficents (smallest index first)
        elif isinstance(coefficients, list):
            dict.__init__(self, {i: c for i, c
                                 in enumerate(coefficients) if c})
        # Polynom given by a dict of {index:coefficient}
        elif isinstance(coefficients, dict):
            dict.__init__(self, {i: c for i, c
                                 in coefficients.items() if c})
        # Polynom is a monom given only by its degree
        elif isinstance(coefficients, int):
            dict.__init__(self, {coefficients: 1})

    def __eq__(self, other):
        if isinstance(other, Polynom):
            return dict(self) == dict(other)
        elif isinstance(other, int):
            return self.constant() and self.constant_part() == other
        return False

    def _show(self, short=False):
        """This is a helper function for __show__ and __repr__"""
        if self.trivial():
            return "0"
        elif self.constant():
            return str(self.constant_part())
        if short:
            # This is a unicode character that might not work in every
            # setup.
            mult = "·"
        else:
            mult = "*"
        string = ""
        # for the first summand to be written, a leading plus can be
        # ommited
        first_summand = True
        # for havng higher degree monoms last, just toggle the
        # reverse-argument
        for i, coefficient in sorted(self.items(), reverse=True):
            sig = ""
            if coefficient < 0:
                sig = "-"
            elif first_summand:
                sig = ""
            else:
                sig = "+"
            coefficient = abs(coefficient)
            if coefficient == 0 and short:
                # coefficient is 0, don't write term
                pass
            elif coefficient == 1 and i == 1 and short:
                # either +x or -x
                string += "{}x".format(sig, i)
                first_summand = False
            elif coefficient == 1 and i == 0 and short:
                # either +1 or -1
                string += "{}1".format(sig, i)
                first_summand = False
            elif coefficient == 1 and short:
                # coefficient is 1, don't write it (only its signum)
                string += "{}x^{}".format(sig, i)
                first_summand = False
            elif i == 0 and short:
                # constant part of polynom
                string += sig + str(coefficient)
                first_summand = False
            elif i == 1 and short:
                # linear part of polynom
                string += "{}{}{}x".format(sig, coefficient, mult)
                first_summand = False
            else:
                string += "{}{}{}x^{}".format(sig, coefficient, mult, i)
                first_summand = False
        return string

    def __repr__(self):
        return self._show(short=False)

    def __str__(self):
        return self._show(short=True)

    def copy(self):
        return Polynom(self)

    def __call__(self, x_value, mod=None):

        """Evaluate the polynom at a certain point
        (eventually modulo mod)
        """
        # The initial y_value, before all the monomes are added
        # to it. It has value 0, but in case the polynom self is
        # trivial we want y_value to have the same type like
        # x_value. Otherwise we could just have written: y_value
        # = 0 This is the sort of trickery you get with a dynamic
        # type system.
        y_value = 0 * x_value

        for i, coefficient in self.items():
            y_value += coefficient * pow(x_value, i, mod)
        if mod is not None:
                y_value = y_value % mod
        return y_value

    def composite(self, other):
        """composition of two polynoms
        (f.composite(g))(x) == f(g(x))
        """
        return self(other)

    def __missing__(self, key):
        """
        Default value for coefficients
        """
        if isinstance(key, int) and key >= 0:
            return 0
        raise TypeError(key)

    def __setitem__(self, key, value):
        """This is the function that is used when we call pol[4] = 16.

        pol = Polynom({1:1, 2:4, 3:9, 4:15})
        pol[4] = 16
        pol[5] = 25
        print(pol) # --> 1x + 4x^2 + 9x^3 + 16x^4 + 25x^5
        pol == Polynom({1:1, 2:4, 3:9, 4:16, 5:25}) # --> True
        """

        if value == 0:
            # In this case, remove the key(if it exists) and do
            # nothing else.
            if self.__contains__(key):
                self.__delitem__(key)
        else:
            # Every nonzero coefficient is saved to the dictionary.
            dict.__setitem__(self, key, value)

    # basic functionality

    def degree(self):
        """The degree of the polynom.

        -1 for the trivial polynom (the one where
        all coefficents are 0)
        """
        return max(self.keys(), default=-1)

    def leitcoefficient(self):
        """Coefficient of the highest degree monom in the polynom."""
        if self.trivial():
            return 0
        return self[self.degree()]

    def constant_part(self):
        """Coefficient of the degree 0 in the polynom."""
        return self[0]

    def trivial(self):
        """a polynom is trivial if all coefficients are 0 (so in a way it has no coefficients).

        This _could_ be implemented using len(self), but I am not sure
        if len(self) is usefull method for a polynom.

        """
        return self.degree() == -1

    def constant(self):
        """True iff the polynom is constant."""
        return self.trivial() or self.degree() == 0

    def monic(self):
        """True iff the polynom is monic (has leitcoefficient 1)"""
        return self.leitcoefficient() == 1

    # basic arithmetic

    def __add__(self, other):
        # Polynom + Int = Polynom
        if isinstance(other, int):
            pol = self.copy()
            pol[0] += other
            return pol
        # Polynom + Polynom = Polynom
        elif isinstance(other, Polynom):
            pol = self.copy()
            for i in other.keys():
                pol[i] += other[i]
            return pol
        else:
            raise TypeError

    __radd__ = __add__

    def __mul__(self, other):
        # Polynom * Integer = Polynom
        if isinstance(other, int):
            return Polynom({i: other * c for i, c in self.items()})
        # Polynom * Polynom = Polynom
        if isinstance(other, Polynom):
            pol = Polynom({})
            for index1, coefficient1 in self.items():
                for index2, coefficient2 in other.items():
                    pol[index1+index2] += coefficient1 * coefficient2
            return pol

    __rmul__ = __mul__

    def __neg__(self):
        # pol.__neg__() should be the same as Polynom({}) - pol
        return self * (-1)

    def __sub__(self, other):
        return self + other.__neg__()

    def __abs__(self):
        # make the leitcoefficient positive
        if self.leitcoefficient() < 0:
            return -self
        else:
            return self

    def __pow__(self, exponent, modulo=None):
        """
        Calculate (self ** exponent) % mod
        exponent is an integer, modulo is another polynom or a field
        """
        assert isinstance(exponent, int)
        assert exponent >= 0

        result = monom(0)       # constant Polynom 1
        tmp_pot = self
        while exponent != 0:
            if exponent % 2 == 1:
                result = (result * tmp_pot) % modulo
            exponent = exponent // 2
            # Only do this if we need tmp_pot later, since multiplying
            # is somewhat expensive (especially if modulo is None)
            if exponent != 0:
                tmp_pot = (tmp_pot * tmp_pot) % modulo
        return result

    def affin_linear_root(self, field_characteristic=0):
        """Tells the root of a linear polynom"""
        if field_characteristic == 0:
            if self.degree() == 1:
                # Polynom has the form a*x+b where a != 0
                linear_part = self[1]
                constant_part = self[0]
                if constant_part % linear_part == 0:
                    return -constant_part // linear_part
                else:
                    # There is no integer root
                    return None
            elif self.trivial():
                # Technically every integer is a root
                # So what should I return?
                raise ValueError
            else:
                raise ValueError

        else:
            poly = self % field_characteristic

            if poly.degree() == 1:
                return (-poly.constant_part() *
                        inverse_in_finite_field(poly.leitcoefficient(), field_characteristic)
                       ) % field_characteristic
            else:
                raise ValueError

    def __divmod__(self, divisor, field_characteristic=0):
        """Used when divmod(pol1, pol2) is called, there is also an optional
        argument which can be called by polynom.divmod().
        """
        rest_to_divide = self.copy() % field_characteristic
        quotient = Polynom([])
        while rest_to_divide.degree() >= divisor.degree():
            # At this point we should have:
            # self == quotient * divisor + rest_to_divide
            quotient_part = rest_to_divide._leitmonom_division(divisor, field_characteristic)
            if quotient_part.trivial():
                break
            quotient += quotient_part
            rest_to_divide = (rest_to_divide - quotient_part * divisor) % field_characteristic
        return (quotient, rest_to_divide)

    # Helper for polynom division

    def _leitmonom_division(self, other, field_characteristic=0):
        """This is a helper funktion for divmod.
        It divides polynoms, but only their highest rank coefficients.
        Called with the arguments
        5 + 6*x + 7 * x*x + 8 * x*x*x and
        3 + 4*x
        will return 2 * x*x
        because  2*x*x * 4*x == 8*x*x*x
        """
        degree_diff = self.degree() - other.degree()
        if degree_diff < 0:
            return Polynom({})
        if field_characteristic != 0:
            coefficient = self.leitcoefficient() * inverse_in_finite_field(other.leitcoefficient(), field_characteristic) % field_characteristic
        else:
            coefficient = frac.Fraction(self.leitcoefficient(), other.leitcoefficient())
        return monom(degree_diff, c=coefficient)

    def __floordiv__(self, other):
        """Used when pol1 // pol2 is called."""
        quotient, _ = divmod(self, other)
        return quotient

    def divmod(self, other, field_characteristic = 0):
        """wrapper for __divmod__, but with an additional optional argument
        field_characteristic (since standard divmod can't take an
        optional third argument.)
        """
        return self.__divmod__(other, field_characteristic)

    def is_divided_by(self, divisor):
        """True if there exists a polynom p so that p * divisor = self"""
        return (self % divisor).trivial()

    def __mod__(self, other):
        """Reduce as much as possible from the polynom. With no additional
        arguments this gets you just a copy of the original. Arguments
        can be integers or Polynoms. If other is None the Polynom will
        be returned unaltered.

        """
        field_characteristic = 0
        mod_polynom = None

        if isinstance(other, int):
            field_characteristic = other
        elif isinstance(other, Polynom):
            mod_polynom = other
        elif isinstance(other, f.Field):
            field_characteristic = other.characteristic
            mod_polynom = other.splitting_polynom

        pol = self.copy()
        if field_characteristic is not None and field_characteristic != 0:
            pol = Polynom({i: c % field_characteristic for i, c in pol.items()})
        if mod_polynom is not None:
            _, pol = pol.divmod(mod_polynom, field_characteristic)
        return pol

    def is_irreducible_over_F_p(self, prime):
        """True if the polynom is irreducible over Z/pZ,
        This assumes that prime is a prime number and self is monic.

        This is a nonoptimized version of the Rabin test.
        """
        assert(self.monic())

        d = self.degree()
        d_prime_factors = trying.unique_factor_list(d)

        # This is a complicated way of testing that self divides
        # x^(p^d)-x in F_p
        field = f.Field(prime, self)
        if pow(Polynom(1), prime ** d, field) != Polynom(1):
            return False
        for factor in d_prime_factors:
            help_pol = pow(Polynom(1), prime ** (d // factor), field) - Polynom(1)
            # If help_pol is trivial we will get
            # gcd(self, help_pol) == self
            # (which might be a flaw in the implementiation or in the
            # definition of the gcd) In this case the test clearly
            # failed since self divides x^(d//factor)-x
            gcd_self_help_pol = gcd(self, help_pol, field_characteristic=prime)
            if not gcd_self_help_pol.constant() or help_pol.trivial():
                return False
        return True

    def der(self):
        """Derive the polynom"""

        # This is only a oneliner because our __init__ function
        # doesn't save keys with value 0. Otherwise we would get a
        # -1:0 entry. And now, thanks to comments, it isn't a oneliner
        # anymore.
        return Polynom({i-1: i*coefficient for i, coefficient in self.items()})

    def roots(self, mod_int, already_stripped_down=False, indentation=""):
        """Find roots of self modulo mod_int.
        mod_int should be a prime number.

        mod = 0 (root finding in Z) is not implemented, this might be
        done with Gauss lemma (a root of a monic polynom with integer
        coefficients always divides the constant part) or just
        Newton's method and polynom division.

        """
        log.debug(indentation +
                  "# looking for roots of {} mod {}".format(self, mod_int))
        indentation = indentation + "  "
        if mod_int == 2:
            # The only even prime gets a special treatment
            rootlist = []
            for potential_root in [0,1]:
                if self(potential_root) % mod_int == 0:
                    rootlist.append(potential_root)
            return rootlist
        elif mod_int == 0:
            raise NotImplementedError

        # Stripping down the polynom
        if already_stripped_down:
            g = self.copy()
        else:
            # all_roots_polynom: x^(mod_int) - x
            all_roots_polynom = monom(mod_int) - monom(1)
            g = gcd(self % mod_int, all_roots_polynom, mod_int)
            log.debug(indentation +
                      "Stripped down polynom to {} (all roots preserved, but multiplicity is 1 now)".format(g))

        # Test base cases where we can see the roots directly. This
        # happens if g.degree <= 1.
        if g.trivial():
            log.debug(indentation +
                      "Polynom is trivial, every number from 0 to {} is a root.".format(mod_int-1))
            return range(mod_int)
        elif g.constant():
            log.debug(indentation +
                      "Polynom is constant {}, so there is no root.".format(g))
            return []
        elif g.degree() == 1:
            root = g.affin_linear_root(mod_int)
            log.debug(indentation +
                      "Polynom is linear, so there is exactly 1 root: {}".format([root]))
            return [root]
        # I guess we could also directly solve this for g.degree()==2,
        # but we don't need to. (It would probably be even faster,
        # though).

        log.debug(indentation + "No base case occured, so we try to shift and split the polynom.")

        for b in range(mod_int):
            # Shifting g by b, so maybe we can do a usefull split g(x)
            # --> g(x-b) = g_0(x) * pol1(x) * pol2(x) Of course, when
            # we know the roots of g o (x-b), we can get the roots of
            # g very easy by substracting b from them
            g_xmb = g.composite(Polynom({0:-b, 1:1})) % mod_int
            rootlist_b = []

            # We know that g_xmb divides x^mod - x and (since mod is
            # odd):
            # x^mod -x = x*(x^((mod-1)/2) + 1)*(x^((mod-1)/2) - 1),
            # where all the three factors are coprime. So we can
            # instead search for roots of
            g_0 = gcd(g_xmb, monom(1), mod_int)
            g_1 = gcd(g_xmb, monom((mod_int-1)//2) + monom(0), mod_int)
            g_2 = gcd(g_xmb, monom((mod_int-1)//2) - monom(0), mod_int)
            # and still find all the roots of gxmb. Recursion!

            # See if that split helped us, try another one if not.
            if g_xmb.degree() == max(g_1.degree(), g_2.degree()):
                log.debug(indentation + "Useless shift by {} from {} to {}".format(b, g, g_xmb))
                pass
            else:
                if b == 0:
                    log.debug(indentation + "No shift necessary.")
                else:
                    log.debug(indentation + "Helpfull shift by {} from {} to {}".format(b, g, g_xmb))
                log.debug(indentation + "{} splits into ({})*({})*({})".format(g, g_0, g_1, g_2))

                g_0_roots = g_0.roots(mod_int, True, indentation)
                g_1_roots = g_1.roots(mod_int, True, indentation)
                g_2_roots = g_2.roots(mod_int, True, indentation)
                rootlist_b = g_0_roots + g_1_roots + g_2_roots
                rootlist = [(x - b) % mod_int for x in rootlist_b]

                log.debug(indentation + "Roots of {} mod {}: {}".format(g_xmb, mod_int, rootlist_b))
                log.debug(indentation + "Roots of {} mod {}: {}".format(g, mod_int, rootlist))

                return rootlist

        raise Exception("I should never be here")

    def square_root_in_Z(self):
        """This assumes that self is a perfect square"""
        log.debug("Searching the square_root of {}.".format(self))
        if self.trivial():
            return Polynom()
        else:
            assert self.degree() % 2 == 0
        root = Polynom({self.degree() // 2 :
                        ba.isqrt(self.leitcoefficient())
                       })

        rest = self - root**2
        rest_degree_old = rest.degree()
        while not rest.trivial():
            log.debug("We know that root starts with {}, which leaves us with a rest of {}.".format(root, rest))
            assert rest.leitcoefficient() % (2 * root.leitcoefficient()) == 0
            next_term_degree = rest.degree() - root.degree()
            next_term_leitcoefficient = rest.leitcoefficient() // (2 * root.leitcoefficient())
            rest_root_leitmonom = Polynom({next_term_degree:next_term_leitcoefficient})
            log.debug("The next term of the root is {}.".format(rest_root_leitmonom))
            root += rest_root_leitmonom
            rest = self - root**2
            assert rest.degree() < rest_degree_old
        return root

    def square_root_in_finite_field(self, field, xi_strategy = "tonelli_shanks"):
        square = self % field
        if square.trivial():
            return Polynom()
        log.debug("Searching a square root of {} (={}) in {}".format(self, square, field))
        s, r = co.divide_out((field.characteristic ** field.splitting_polynom.degree())-1, 2)
        assert s > 1 # We can't handle field_characteristic 2 yet.
        assert s * (2 ** r) + 1 == field.characteristic ** field.splitting_polynom.degree()

        # We will find a base for S_(2^r), the Sylow 2-subgroup
        non_residue = field.quadratic_non_residue()
        sylow_group_generator = pow(non_residue, s, field)
        log.debug("The Sylow group S_(2^{}) is generated by {}".format(r, sylow_group_generator))

        omega = pow(square, (s+1)//2, field)
        square_pow_s = pow(square, s, field)

        # There are multiple ways to calculate the square root we
        # need.

        if xi_strategy == "brute_force":
            # This is the simpler way which will be slow for big
            # numbers (since the elements of the Sylow group are
            # calculated explicitly.)

            sylow_group = []
            xi = None
            for i in range(2**r):
                sylow_element = pow(sylow_group_generator, i, field)
                sylow_group.append(sylow_element)
                if pow(sylow_element, 2, field) == square_pow_s:
                    xi = sylow_element
                    find_whole_Sylowgroup = False
                    if find_whole_Sylowgroup:
                        # break here if you don't need to output the complete Sylow group
                        break
            if xi is None:
                raise ArithmeticError
            log.debug("S_{}: {} (generated by {})".format(2**r, sylow_group, sylow_group_generator))
            log.debug("xi: {}".format(xi))
            square_root = (omega * inverse_in_finite_field(xi, field)) % field
        elif xi_strategy == "tonelli_shanks":
            # We relax the formula (root^2 = square) to:
            # (quasi_root^2=square*lamb) where order(lamb)=2^m divides
            # 2^(r-1). Then we produce a series of (quasi_root,lamb,m)
            # that fulfill this condition, and each m is smaller then
            # the one before. So at some point we will have m=0, which
            # means that lamb=1 and quasi_root^2=square*1=square.

            lamb = square_pow_s
            quasi_root = omega
            m = r-1
            while lamb != Polynom({0:1}):
                while pow(lamb, pow(2, m-1), field) == Polynom({0:1}):
                    m = m-1
                log.debug("quasi_root: {}, lambda: {}, m: {}, r: {}".format(quasi_root, lamb, m, r))
                assert pow(quasi_root, 2, field) == (lamb * square) % field
                quasi_root *= pow(sylow_group_generator, pow(2, r-m-1), field)
                lamb *= pow(sylow_group_generator, pow(2, r-m), field)
                quasi_root, lamb = quasi_root % field, lamb % field
            square_root = quasi_root
        else:
            raise ValueError
        log.debug("Found root of {} in {}: {}".format(self, field, square_root))
        return square_root

def inverse_in_finite_field(a, field):
    """Find b in the finite_field so that a * b = 1 (mod field). If field
    is an integer it is assumed prime and we are using the field with
    that many elements in it.
    """
    if isinstance(field, int):
        if isinstance(a, int):
            # That field_int better be a prime.
            g, b, _ = extended_gcd(a, field)
            if g == 1:
                return b % field
            else:
                raise ValueError
        if isinstance(a, Polynom):
            return inverse_in_finite_field(a, f.Field(field))
    elif isinstance(a, Polynom) and isinstance(field, f.Field):
        assert(not (a % field).trivial())
        g, r, _ = extended_gcd(a, field.splitting_polynom, field.characteristic)
        assert (g % field).constant()
        g_inv = inverse_in_finite_field(g[0], field.characteristic)
        inverse = (r * g_inv) % field
        assert (inverse * a) % field == Polynom({0:1})
        return inverse

def monom(degree, c = 1):
    return Polynom({degree:c})

def gcd(p,q, field_characteristic = 0):
    """Calculate the gcd of two integers/polynoms
    """
    return extended_gcd(p, q, field_characteristic)[0]

def extended_gcd(p, q, field_characteristic = 0):
    if isinstance(p, Polynom) and isinstance(q, Polynom):
        if q.trivial():
            return (p,Polynom({0:1}),Polynom({}))
        else:
            p_div_q, p_mod_q = p.divmod(q, field_characteristic)
            d, s_helper, t_helper = extended_gcd(q, p_mod_q, field_characteristic)
            s, t = t_helper, s_helper - p_div_q * t_helper
            return d, s, t
    elif type(p) == type(q) == int and field_characteristic == 0:
        x0, x1, y0, y1 = 1, 0, 0, 1
        while q != 0:
            quotient, p, q = p // q, q, p % q
            x0, x1 = x1, x0 - quotient * x1
            y0, y1 = y1, y0 - quotient * y1
        return  p, x0, y0
    else:
        raise TypeError
