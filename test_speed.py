import time
import common as co
import logging
import polynom as pol
import field as f
import primelist
import briggs_example as brex
import sieves as sv

co.setup_logger()
log = logging.getLogger("test_speed_logger")

# Prepare test
r_factor_base = brex.rfb
a_factor_base = brex.afb
number_of_smoothies_necessary = brex.number_of_smoothies_necessary
poly = brex.poly
d = brex.d
m = brex.m


# Start test
start = time.process_time()

# Testing happens here

result = sv.gnfs_sieve(r_factor_base, a_factor_base, poly, d, m, b=1)

# Stop test
stop = time.process_time()

# Test result (optional)
expected_result = [(a,b) for (a,b) in brex.smoothies if b == 1]
if set(expected_result).issubset(set(result)):
    log.info("Found all the smoothies.")
else:
    log.info("Didn't find all the smoothies.")
if expected_result is None:
    pass
elif expected_result == result:
    log.info("Result is as expected")
else:
    log.info("Result is NOT as expected")    
    log.info("expected Result: {}".format(expected_result))    

#Report result
log.info("Result: {}".format(result))
log.info("Programm needed {} seconds to run.".format(stop - start))
