import common as co
import exceptions as ex
import primelist

def non_trivial_factor(n):
    for p in primelist.primelist:
        if n == p:
            return None
        if n % p == 0:
            return p
    raise ex.NoFactorFound(n)

def unique_factor_list(n):
    # Note that for n prime this will return [n] (or None if n is too large)
    l = []
    for p in primelist.primelist:
        n, multiplicity = co.divide_out(n,p)
        if multiplicity >= 1:
            l.append(p)
        if n == 1:
            return l
    # If we are here, we were not able find all factors
    return None
