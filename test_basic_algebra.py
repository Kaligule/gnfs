import unittest
from math import log, floor
import hypothesis.strategies as st
from hypothesis import given, example
import hypothesis_strategies as hyst
import basic_algebra as ba

class TestBasicAlgebra(unittest.TestCase):

    @given(st.integers(min_value = 0))
    def test_isqrt(self, x):
        n = x * x
        y = ba.isqrt(n)
        assert pow(y, 2) == n
        assert 0 <= y <= n
        assert x == y

        if x != 1:
            self.assertRaises(ValueError, ba.isqrt, n-1)

    @given(st.integers(min_value = 0, max_value = 10000),
           st.integers(min_value = 2),
    )
    def test_convert_to_base(self, n, base):
        digit_list = ba.convert_to_base(n, base)
        if base > abs(n):
            assert digit_list == [n]
        else:
            assert len(digit_list) > 1
        if n == 0:
            # hard coded special case
            assert digit_list == [0]
        else:
            assert len(digit_list) == int(1 + floor(log(n, base)))
        n_recalculated = 0
        for exponent, digit in enumerate(digit_list):
            assert 0 <= digit < base
            n_recalculated += digit * base ** exponent
        assert n == n_recalculated

    @example({7027: 2183, 7039: 648, 7057: 3232, 7079: 4348, 7151:
              131, 7213: 3065, 7219: 3634, 7229: 4546, 7237: 4512,
              7297: 6061, 7333: 4708, 7369: 3878, 7477: 6603},
             10
    )
    @example({7297: 6061, 7333: 4708, 7039: 648, 7079: 4348, 7369:
                  3878, 7213: 3065, 7151: 131, 7057: 3232, 7219: 3634,
                  7477: 6603, 7027: 2183, 7229: 4546, 7237: 4512 },
             10
    )
    @given(st.dictionaries(keys = hyst.primes(),
                           values = st.integers(),
                           average_size=10,
                          ),
           st.integers(min_value = 1),
    )
    def test_chinese_remainder_theorem(self, dictionary, modulo):
        product_of_all_keys = ba.product(dictionary.keys())

        result = ba.chinese_remainder_theorem(dictionary)
        result_modulo_keys = ba.chinese_remainder_theorem(dictionary, product_of_all_keys)
        result_modulo = ba.chinese_remainder_theorem(dictionary, modulo)
        # assert result is correct
        for key, value in dictionary.items():
            assert result % key == value % key
        # assert result is minimal
        assert result == result % product_of_all_keys == result_modulo_keys
        # check modulo
        # assert result % modulo == result_modulo
