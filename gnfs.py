import yaml
import logging.config

import choose_parameters as cp
import primelist
import fractions as frac
import exceptions as ex
import polynom as pol
import field as f
import common as co
import basic_algebra as ba
import briggs_example as brex
import sieves as sv
import build_matrix as bm

def gnfs(n):
    # logger
    co.setup_logger()
    log = logging.getLogger('gnfs_logger')

    # Briggs Chapter 5.1
    # choosing poly, d and m
    log.debug("Chapter 5.1")
    log.info("Trying to find a nontrivial factor of n = {}".format(n))
    poly, d, m = cp.choose_parameters(n, "base-m-reduced")
    log.info("Chosen polynom: poly(x)={}, degree d = {}".format(poly, d))
    assert poly(m) % n == 0
    log.info("Chosen m: m = {}, so poly(m) % {} == 0".format(m, n))

    # Determine the sizes of the factorbases and characterbase upfront.
    rfb_size, afb_size, qcb_size = cp.choose_factorbase_sizes(n, poly, m)

    # Briggs Chapter 5.2
    # choosing a rational factorbase
    log.debug("Chapter 5.2")
    rfb = [(m % p, p) for p in primelist.primelist[:rfb_size]]
    log.info("Size of rational factor base: {}".format(len(rfb)))
    log.debug("Rational factor base: {}".format(rfb))

    # Briggs Chapter 5.3
    # choosing an algebraic factorbase
    log.debug("Chapter 5.3")
    # We need prime ideals for the algebraic factorbase and the
    # quadratic character base.
    prime_ideals = co.prime_ideals(poly, afb_size + qcb_size)
    afb = prime_ideals[:afb_size]
    log.info("Size of algebraic factor base: {}".format(len(afb)))
    log.debug("Algebraic factor base: {}".format(afb))

    # Briggs Chapter 5.4
    # choosing an quadratic character base
    log.debug("Chapter 5.4")
    qcb = prime_ideals[afb_size:afb_size + qcb_size]
    log.info("Size of quadratic character base: {}".format(len(qcb)))
    log.debug("Quadratic character base: {}".format(qcb))

    # Briggs Chapter 5.5
    log.debug("Chapter 5.5")

    number_of_smoothies_necessary = 1 + len(rfb) + len(afb) + len(qcb)
    log.info("We will need at least {} smoothies".format(
        number_of_smoothies_necessary))

    # sieving
    smoothies = sv.gnfs_sieve(rfb, afb, number_of_smoothies_necessary, poly, m,)
    log.info("Number of smoothies found: {}".format(len(smoothies)))
    log.debug("Smoothies found: {}".format(smoothies))

    # Briggs Chapter 5.6
    # constructing binary matrix
    log.debug("Chapter 5.6")
    boolean_matrix = bm.smoothies_to_matrix(smoothies, rfb, afb, qcb, m, poly,)
    log.info("{}-matrix constructed".format(boolean_matrix.shape))

    # Briggs Chapter 5.7
    # finding Dependencies
    log.debug("Chapter 5.7")
    kernel = co.boolean_kernel(boolean_matrix)
    log.info("kernel has dimension {}".format(len(kernel)))

    # list that holds the lists of smoothies comming from a kernel
    # vector.
    dependend_smoothie_list = [[smoothie for smoothie, is_in_kernel
                                         in zip(smoothies, kernel_vector)
                                         if is_in_kernel]
                               for kernel_vector
                               in kernel]

    for dependend_smoothies in dependend_smoothie_list:
        log.debug("These smoothies are linearly dependend: {}".format(
            dependend_smoothies))

    # We choose one set of dependend smoothies to work with. If it
    # fails, we can just use the next one
    num_no_squares_produced = 0
    num_no_factor_produced_from_squares = 0
    for smoothie_list in dependend_smoothie_list:
        log.info("These are smoothies we will work with for now: {}".format(
            smoothie_list))

        # Briggs Chapter 5.8
        # Computing An Explicit Square Root
        log.debug("Chapter 5.8")
        # In Briggs's paper this chapter is mainly for doublechecking
        # and beta is revealed directly (by just knowing). We can't do
        # that in the real algorythm. We still calculate most of the
        # missing variables.

        # Calculating y
        product_of_smooth_ints = ba.product([a + b * m for a,b in smoothie_list])
        log.info("Product over a+b*m for all (a,b) in the dependend smoothie list is {}".format(product_of_smooth_ints))
        y = ( (poly.der())(m) * ba.isqrt(product_of_smooth_ints) ) % n
        log.info("y = {}".format(y))

        # Calculating delta (and its norm)
        delta = ba.product([pol.Polynom({0:constant_part,1:linear_part})
                            for constant_part, linear_part in smoothie_list]) % poly
        norm_delta = ba.product([ co.norm(constant_part, linear_part, poly)
                            for constant_part, linear_part in smoothie_list])
        log.info("Found delta to be {} (norm {}).".format(delta, norm_delta))

        # Calculating beta^2
        beta_square = pow(poly.der(), 2) * delta
        log.info("beta^2 = {}.".format(beta_square))

        # Briggs Chapter 5.9
        # Determining Applicable Finite Fields
        log.debug("Chapter 5.9")
        field_primes_lower_bound = 7000
        field_primes = []
        product_of_fieldprimes = 1
        for prime in filter(lambda p: p > field_primes_lower_bound,
                            primelist.primelist):
            if poly.is_irreducible_over_F_p(prime):
                field_primes.append(prime)
                product_of_fieldprimes *= prime
                # This is kind of arbitrary, but worked until now.
                if product_of_fieldprimes > n ** 7:
                    break
        assert product_of_fieldprimes > n**4, "Didn't find enough compatible primes. Found only these: {}.".format(field_primes)
        log.info("Compatible primes: {}".format(field_primes))
        log.info("The product of these is {}".format(product_of_fieldprimes))

        sqrt_norm_delta = ba.isqrt(norm_delta)
        log.debug("The squareroot of delta should have norm {}.".format(
            sqrt_norm_delta))

        beta_dict = {}
        for prime in field_primes:
            field = f.Field(prime, poly)
            log.info("We are working in the field {} for now.".format(field))

            # we can finaly find roots of beta^2 (even if it is only
            # in the finite field)
            beta_p = beta_square.square_root_in_finite_field(field)
            norm_beta_p = field.norm(beta_p)
            log.debug("    A squareroot of beta_{} is {} (with norm {}).".format(prime ,beta_p, norm_beta_p))
            # We might have found -beta_p instead of beta_p,
            # though. We will know by the norm.

            # Expected norm of sqrt(beta^2)
            norm_beta_p_expected = (field.norm(poly.der()) * sqrt_norm_delta) % prime
            log.debug("    Norm(beta_p) is expected to be {}.".format(prime, norm_beta_p_expected))

            # if we got the wrong squareroot of beta^2 we will change it here.
            if norm_beta_p == norm_beta_p_expected:
                log.debug("    Since the norm is as expected we will go for this root.")
            elif norm_beta_p == -norm_beta_p_expected % prime:
                beta_p = (-beta_p) % field
                norm_beta_p = field.norm(beta_p)
                log.debug("    This is the negative root, so we will go invert it to for {} (with norm {}) instead.".format(beta_p, norm_beta_p))
            else:
                raise Exception("Calculation of square roots went wrong.")
            log.debug("    The projection of beta into {} is {} (with the expected norm {}).".format(field, beta_p, norm_beta_p))
            beta_dict[field.characteristic] = beta_p

        x_remainder_dict = {prime: int(beta(m)) % prime
                          for prime, beta in beta_dict.items()}
        log.info("These are the remainders (x % prime) of x for the given primes: {}".format(x_remainder_dict))

        # Briggs Chapter 5.11
        # Using the chinese remainder theorem
        log.debug("Chapter 5.11")
        x = ba.chinese_remainder_theorem(x_remainder_dict) % n
        log.info("x = {}".format(x))

        # Check we have a difference of squares
        if pow(x, 2, n) != pow(y, 2, n):
            # Bad luck.
            log.info("The squares of {} and {} aren't equal modulo {}.".format(x, y, n))
            num_no_squares_produced += 1
            num_no_factor_produced_from_squares = 0

            continue
        log.info("y^2 = {} = {} = x^2 (mod {})".format(pow(x, 2), pow(y, 2), n))

        # Briggs Chapter 5.12
        # Putting it all together
        log.debug("Chapter 5.12")
        factor = co.difference_of_squares(n, x, y)
        if factor is not None:
            assert n % factor == 0
            log.info("{} = {} * {}".format(n, factor, n // factor))
            return factor

        else:
            # No factor from this set of dependend smoothies
            num_no_factor_produced_from_squares += 1
            log.info("Bad luck, we will have to try another pair of smoothies.")
    # If all sets fail
    log.warning("No factor of {} was found. We failed to produce the squares {} times and to produce a factor from them {} times.".format(n, num_no_squares_produced, num_no_factor_produced_from_squares))
    raise ex.StrategyFailed
