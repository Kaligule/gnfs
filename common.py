#!/usr/bin/python3

import fractions as frac
import numpy as np
import primelist
import tonelli_shanks as ts
import logging.config
import yaml
import os


def setup_logger():
    """
    Set up all the loggers for the different modules.

    Just call this function and then get the logger you need.
    """
    with open('logging.yaml') as f:
        os.makedirs("log", exist_ok=True)
        D = yaml.load(f)
        logging.config.dictConfig(D)

def difference_of_squares(n, root1, root2):
    """
    Input:
    n: An integer to factorize
    root1 and root2: Two integers so that root1 ** 2 = root2 ** 2 (mod n)

    Output:
    A factor of n if one is found, None else.
    """

    log = logging.getLogger("common_logger")
    assert pow(root1, 2, n) == pow(root2, 2, n), "The squares aren't equal modulo {}.".format(n)

    candidate_1 = frac.gcd(root1 - root2, n)
    candidate_2 = frac.gcd(root1 + root2, n)

    if n % candidate_1 == 0 and candidate_1 != 1 and candidate_1 != n:
        return candidate_1
    elif n % candidate_2 == 0 and candidate_2 != 1 and candidate_2 != n:
        return candidate_2
    else:
        return None

def dict_dict_to_matrix(dictionary, f=lambda x: x, first_keys=None, second_keys=None):
    """
    Input:
    dictionaty: dict which maps a key x to a dict wich maps a key y to a value z of type t
    f: a function wich takes values of type t
    Output: A matrix:


                              first key x
                 +-----------------+----------------->
                 |
                 |
    second key y +          f(dictionary[x][y])
                 |
                 |
                 V

    """

    if not first_keys:
        first_keys = dictionary.keys()
    if not second_keys:
        second_keys = (next(iter(dictionary.values()))).keys()

    m = np.matrix(np.empty(shape = (len(second_keys), len(first_keys)),dtype = bool))

    for  s_index, s in enumerate(first_keys):
        for p_index, p in enumerate(second_keys):
            # print("{} (number {} in the primelist) divides {} (number {} in the smooth_number_dict) {} times.".format(p, p_index, (s**2) % n, s_index, smooth_number_dict[s][p]))
            m[p_index, s_index] = f(dictionary[s][p])
    return m

def is_smooth(potential_smoothie, factorbasis, m = None, poly = None):
    """

    Test potential_smoothie for (rational or algebraic) smoothness.

    If it is smooth then is_smooth returns a factordict of the form
    {primeelement : multiplicity}. If potential_smoothie is not smooth,
    just return None.

    What smothness is tested depends on the the type of the
    potential_smoothie:

    - If potential_smoothie is an int, rational smoothness is looked
    at (since there is no use for m and poly in this case we make sure
    they are not set). The factorbasis may contain primes or tuples of
    the form (int, prime) (the int will be ignored, though)

    - If potential_smoothie is a tuple (a,b) of two ints, algebraic
      smoothness is tested. Since we need to know m and poly to know
      what primeideal the tuples represent, make sure they are set
      correctly.

    """
    # TODO m is actually never needed/used in this test, so we should get rid of the argument.
    factordict = {}
    if isinstance(potential_smoothie, int):
        assert m is None
        assert poly is None
        # We are talking about rational smoothness here
        for prime in factorbasis:
            potential_smoothie, multiplicity = divide_out(potential_smoothie, prime)
            factordict[prime] = multiplicity
        if abs(potential_smoothie) == 1:
            return factordict
    elif isinstance(potential_smoothie, tuple):
        # Make (sortof) sure the potential_smoothie is of type (int, int)
        assert all(isinstance(i, int) for i in potential_smoothie)
        # We are talking about algebraic smoothness here
        a, b = potential_smoothie
        n = norm(a, b, poly)
        for (r, p) in factorbasis:
            if (a + b * r)  % p == 0:
                n, multiplicity = divide_out(n, p)
                factordict[(r, p)] = multiplicity
            else:
                factordict[(r, p)] = 0
        if abs(n) == 1:
            return factordict
    else:
        raise TypeError("First argument of is_smooth is supposed to be of type int or (int, int)")
    return None

def boolean_kernel(boolean_matrix):
    """
    m being a boolean matrix, we find its boolean kernel.
    """
    # make a copy so the original matrix isn't touched, also name is shorter.
    m = boolean_matrix.copy()
    zeilen, spalten = np.shape(m)
    pivot_dict = {}
    rows_with_no_pivot = []

    # out pivot element is at line l, row r
    l = 0
    r = 0

    for r in range(spalten):
        # find a pivot element and place it at (l,r)
        row_has_pivot = False
        # Find a line where it the rth element is True and ...
        for zeile in range(l, zeilen):
            if m[zeile, r]:
                # ... if you find it, document it ...
                row_has_pivot = True
                # ... and swap lines l and zeile, so there is a pivot element at m[l,r]
                m[[l, zeile]] = m[[zeile, l]]
                break

        # If there was no pivot element in this row, mark it so and go to the next line
        if not row_has_pivot:
            rows_with_no_pivot.append(r)
        else:
            # There is a pivot element at m[l,r] now.
            pivot_dict[l] = r
            # Now change the lines below the dth row so they are False in the dth column
            for zeile in range(zeilen):
                if not zeile == l and  m[zeile, r]:
                    m[zeile] = m[zeile] ^ m[l]
            l += 1

    kernel = []
    for r in rows_with_no_pivot:
        # Which row will I have to activate for a kernel_vector?
        # The row without pivot element and all those where this row is true.
        kernel_vector = [False]*spalten
        kernel_vector[r] = True
        for my_index in range(zeilen):
            if m[my_index, r]:
                # which row is to be activated?
                kernel_vector[pivot_dict[my_index]] = True

        kernel.append(kernel_vector)

    return kernel

def divide_out(dividend, divisor):
    """Given two numbers dividend and divisor,
    return (rest, multiplicity), where multiplicity is maximal so that

    dividend = rest * divisor ** multiplicity

    divisor is allowed to be of form (int, div), but then the first int
    will be ignored and just the second one will be used. divisor
    should not be -1, 0 or 1 (don't really make sense).

    """
    # log = logging.getLogger("common_logger")
    # log.debug("divide_out was called with {} and {}".format(dividend, divisor))

    # looking at the arguments
    if isinstance(divisor, int):
        pass
    elif isinstance(divisor, tuple):
        # divisor is of form (r,p)
        divisor = divisor[1]
    else:
        raise TypeError("first argument of divide_out is supposed to be of type int or (int, int)")
    if dividend == 0:
        return dividend, 0
    assert not divisor in [-1,0,1]

    # Calculating the result
    multiplicity = 0
    while dividend % divisor == 0:
        dividend = dividend // divisor
        multiplicity += 1
    return dividend, multiplicity

def dependend_smoothies(smoothies, factorbasis, smoothie_function):
    log = logging.getLogger("common_logger")

    smooth_number_dict = {smoothie: is_smooth(smoothie_function(smoothie), factorbasis) for smoothie in smoothies}

    matrix = dict_dict_to_matrix(smooth_number_dict,
                                 f=lambda x: (not x % 2 == 0),
                                 second_keys=factorbasis)

    kernel = boolean_kernel(matrix)

    dependencies = []
    for kernel_vector in kernel:
        dependend_smoothie_list = []
        for index, smoothie in enumerate(smoothies):
            if kernel_vector[index]:
                dependend_smoothie_list.append(smoothie)
        dependencies.append(dependend_smoothie_list)
        log.debug("These smoothies are dependend: {}".format(dependend_smoothie_list))
    return dependencies

def smooth_number_dict_to_factors(n, factorbasis, smooth_number_dict):
    """for a given number n, a factorbasis and a smooth_number_dict,
    try to find a nontrivial factor of n. Return it (or None if none
    is found)"""
    log = logging.getLogger("common_logger")

    matrix = dict_dict_to_matrix(smooth_number_dict,
                                 f=lambda x: (not x % 2 == 0),
                                 second_keys = factorbasis)

    kernel = boolean_kernel(matrix)
    log.info("Number of differences of squares found {}".format(len(kernel)))

    square_factors = []
    for kernel_vector in kernel:
        for index, smooth_number in enumerate(smooth_number_dict.keys()):
            if kernel_vector[index]:
                square_factors.append(smooth_number)
        # We will find two numbers x and y now that hold x^2 ==  y^2 (mod n)
        # The first one is the product of our smooth numbers:
        x = 1
        for s in square_factors:
            x *= s

        # The second one can be found by looking at the prime_factors of the smooth numbers
        y = 1
        for prime in factorbasis:
            sum_of_exponents = 0
            for s in square_factors:
                sum_of_exponents += smooth_number_dict[s][prime]

            # The sum of exponents should be an even number
            # (that is what all the hastle was all about)
            assert sum_of_exponents % 2 == 0
            half_y_exponent = sum_of_exponents // 2
            y *= prime**half_y_exponent

        # With any luck, we can compute a divisor from x and y now.
        maybe_factor = difference_of_squares(n, root1=x, root2=y)
        if maybe_factor:
            return maybe_factor
        # This kernel_vector didn't work out.

    log.info("No factor found.")
    return None

def norm(a, b, poly):
    # log = logging.getLogger("common_logger")
    # log.debug("Calculating the norm of ({},{}) with the polynom {}".format(a,b,poly))
    res_frac = ((-b) ** poly.degree() )*poly(- frac.Fraction(a,b))
    assert res_frac.denominator == 1
    return int(res_frac)

def prime_ideals(splitting_polynom, number_of_ideals_needed):
    prime_ideals = []
    for p in [p for p in primelist.primelist]:
        for r in splitting_polynom.roots(p):
            prime_ideals.append((r,p))
        if len(prime_ideals) >= number_of_ideals_needed:
            return prime_ideals
    raise ValueError("Didn't have enough primes for that many ideals")

def legendre_symbol(x, field, defined_for_2 = False):
    """Tell if x is a quadratic residue in the finite field of polynoms given

    Compute the Legendre symbol a|p using Euler's criterion. p is a
    prime, a is relatively prime to p (if p divides a, then a|p = 0)
    """
    field_cardinality = abs(field)
    if field_cardinality == 2:
        if defined_for_2:
            return 1
        return None

    powerpol = pow(x, (field_cardinality - 1) // 2, field)

    if isinstance(x, int) and isinstance(field, int):
        return -1 if powerpol == field - 1 else powerpol
    # elif isinstance(x, pol.Polynom) and isinstance(field, f.Field):
    else:
        assert not x.trivial()

        if powerpol == 1:
            return True
        elif (powerpol + 1) % field == 0:
            return False
        else:
            # "Problem with calculating the legendre symbol of {} in
            # Z/{}Z mod {}: ({}) ^ (({}-1)//2) = {} ".format(x,
            # field.characteristic, field.splitting_polynom,
            # x,field_cardinality, powerpol))
            raise ArithmeticError
