test:
	ls | awk '/test_/ && !/speed/ && !/gnfs/' | xargs pytest

test_gnfs:
	rm -f log/*.log
	python3 test_gnfs.py

test_specific:
	rm -f log/*.log
	pytest test_basic_algebra.py::TestBasicAlgebra::test_chinese_remainder_theorem
	# pytest test_factorize.py
	# pytest test_factorize.py::TestPolynom::test_square_root_in_finite_field

test_speed:
	rm -f log/*.log
	python3 test_speed.py

look_at_logfiles:
	find . -name '*.log' | xargs wc -l

