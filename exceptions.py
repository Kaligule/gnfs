class FactorFoundEarly(Exception):
    def __init__(self, factor):
        self.factor = factor
        Exception.__init__(self, "Factor found early: {}".format(factor))

class NoFactorFound(Exception):
    def __init__(self, n):
        Exception.__init__(self, "No factor of {} was found.".format(n))

class StrategyFailed(Exception):
    def __init__(self):
        Exception.__init__(self, "Strategy failed")
        
