import hypothesis.strategies as st
import hypothesis_strategies as hyst
from hypothesis import given, note, assume, example
import field as f
import polynom as pol
import numpy as np
import common as co

class TestPolynom:

    def test_init(self):
        p1 = pol.Polynom({0:6, 1:-7, 3:1})
        p2 = pol.Polynom([6,-7,0,1])
        p3 = pol.Polynom(roots = [1,2,-3])
        assert p1 == p2 == p3

    @given(st.lists(elements = st.integers(min_value = -2**5,
                                           max_value = 2**5),
                    min_size = 1,
                    max_size = 5),
           st.integers(min_value = -2**5,
                       max_value = 2**5))
    def test_polynom_evaluation(self, coeffs, value):
        numpy_poly = np.polynomial.Polynomial(coeffs)
        numpy_value = numpy_poly(value)
        own_poly = pol.Polynom(coeffs)
        own_value = own_poly(value)
        assert numpy_value == own_value

    @given(hyst.polynoms(),
    )
    def test_monic(self, poly):
        leitcoefficient = None
        degree = -1
        for key in poly.keys():
            if key > degree:
                degree = key
                leitcoefficient = poly[degree]
        monic = leitcoefficient == 1
        assert poly.monic() == monic

    @given(st.integers(min_value = 0),
           st.integers(),
    )
    def test_monom(self, d, c):
        p = pol.monom(d,c)
        if c == 0:
            assert p.trivial()
        else:
            assert p.degree() == d
            assert p.leitcoefficient() == c
        assert p(1) == c
        assert len(p) == 1 or c == 0
        assert pol.monom(d) == pol.Polynom(d)

    @given(hyst.polynoms(),
           st.integers(min_value=0, max_value=10).map(abs),
           st.integers(min_value=0, max_value=10).map(abs),
           hyst.polynoms(),
    )
    def test_pow(self, p, n, m, mod_polynom):
        assume(not p.trivial or (n > 0 and m > 0))
        assume(not mod_polynom.trivial())
        assert pow(p, n+m) == pow(p, n) * pow(p, m)
        assert pow(p, n+m, mod_polynom) == (pow(p, n, mod_polynom) * pow(p, m, mod_polynom)) % mod_polynom
        assert pow(p, n+m, mod_polynom) == pow(p, n+m) % mod_polynom

    @given(hyst.polynoms(),
           hyst.polynoms(),
           st.integers(),
    )
    def test_polynom_arithmetic(self, pol1, pol2, value):
        # eval is an homomorphism under addition under multiplication
        assert pol1(value) + pol2(value) == (pol1 + pol2)(value)
        assert pol1(value) * pol2(value) == (pol1 * pol2)(value)
        # degree is an homomorphism
        assert pol1.degree() + pol2.degree() == (pol1 * pol2).degree() or pol1.trivial() or pol2.trivial()
        # + and * are commutative
        assert pol1 + pol2 == pol2 + pol1
        assert pol1 * pol2 == pol2 * pol1
        # - is antisymmetric
        assert pol1 - pol2 == -1 * (pol2 - pol1)
        # - is selfinverting
        assert pol1 == -(-pol1)
        # composition
        assert pol1.composite(pol2)(value) == pol1(pol2(value))
        # abs
        assert abs(pol1) in [pol1, -pol1]
        assert abs(pol1).leitcoefficient() >= 0

    @given(hyst.polynoms(),
           hyst.polynoms(),
           hyst.polynoms(),
    )
    def test_polynom_composition(self, pol1, pol2, pol3):
        """
        p1 o (p2 o p3) = (p1 o p2) o p3
        """
        assert pol1.composite(pol2.composite(pol3)) == pol1.composite(pol2).composite(pol3)


    @given(hyst.polynoms(),
           hyst.polynoms(trivial_allowed = False),
    )
    def test_polynom_division(self, pol1, pol2):
        # division
        # take care you don divide by the trivial polynom

        # This might be trivial division quite often
        quotient, rest = divmod(pol1, pol2)
        assert pol2 * quotient + rest == pol1
        if pol1.degree() > pol2.degree():
            assert quotient.degree() > 0

    @given(hyst.polynoms(trivial_allowed = False),
           hyst.polynoms(trivial_allowed = False),
    )
    def test_polynom_division_without_rest(self, pol1, pol2):
        # The easy kind of division, the one without rest.
        assert divmod(pol1 * pol2, pol1) == (pol2, pol.Polynom([]))

        quotient, rest = divmod(pol1, pol2)
        assert (pol1 - rest).is_divided_by(pol2)
        if not quotient.trivial():
            assert (pol1 - rest).is_divided_by(quotient)

        # modulo
        assert ((pol1 * pol2) % pol1).trivial()
        assert ((pol1 * pol2) % pol2).trivial()

    @given(hyst.polynoms(),
           hyst.polynoms(),
    )
    def test_polynom_gcd(self, pol1, pol2):
        g = pol.gcd(pol1, pol2)
        if not g.trivial():
            assert pol1.is_divided_by(g)
            assert pol2.is_divided_by(g)
        h = pol.gcd(pol1*pol1, pol1 * pol2)
        if not pol1.trivial():
            assert h.is_divided_by(pol1)

    def test_is_irreducible_over_F_p(self):
        poly = pol.Polynom([8,29,15,1])
        # Some test cases I took from Briggs' example
        assert poly.is_irreducible_over_F_p(9851)
        assert poly.is_irreducible_over_F_p(9907)
        assert poly.is_irreducible_over_F_p(9929)
        assert not poly.is_irreducible_over_F_p(9923)
        # Two examples I took from here:
        # http://math.stackexchange.com/questions/1343450/how-can-i-prove-irreducibility-of-polynomial-over-a-finite-field
        assert pol.Polynom({0:1, 3:1, 10:1}).is_irreducible_over_F_p(2)
        assert not pol.Polynom([-1,1,1,1,1,1]).is_irreducible_over_F_p(3)
        # Funny thing, x+x^4 isn't reducible in any finite field
        poly = pol.Polynom({1:1, 4:1})
        for prime in hyst.primelist.primelist:
            assert not poly.is_irreducible_over_F_p(prime)

    @given(hyst.polynoms(trivial_allowed=False, min_degree=1),
           hyst.polynoms(trivial_allowed=False, min_degree=1),
           hyst.primes(),
    )
    def test_reducibility(self, pol1, pol2, prime):
        """
        We construct a polynom that is definitely reducible and test
        if this is found by the method is_irreducible_over_F_p.
        """
        # We make the polynoms monic by force. Not the most elegant way, but it works
        pol1[pol1.degree()] = 1
        pol2[pol2.degree()] = 1
        poly = (pol1 * pol2) % prime
        assert not poly.is_irreducible_over_F_p(prime)

    @given(hyst.primes(),
    )
    def test_construct_irreducible_polynom(self, field_characteristic):
        # We are testing only for degree 2 since the test get really slow for higher degrees.
        note("Constructing an irreducible monic polynom over Z/Z*{}".format(field_characteristic))
        poly = f.Field(field_characteristic).splitting_polynom
        assert poly.is_irreducible_over_F_p(field_characteristic)
        assert poly.degree() == 2
        assert poly.leitcoefficient() == 1

    @given(hyst.polynoms(max_degree=3),
           hyst.polynoms(max_degree=3),
           st.integers()
    )
    def test_derivation_rules(self, pol1, pol2, n):
        # derivation is linear
        assert pol1.der() + pol2.der() == (pol1 + pol2).der()
        assert (n * pol1).der() == n * pol1.der()
        # product rule
        assert (pol1 * pol2).der() == pol1.der() * pol2 + pol1 * pol2.der()
        # chain rule
        assert pol1.composite(pol2).der() == pol1.der().composite(pol2) * pol2.der()

    # pol1 is either constant or derivation decreases the degree by 1
    @given(hyst.polynoms())
    def test_derivation_degree(self, pol1):
        if pol1.constant():
            assert pol1.der().trivial()
        else:
            assert pol1.degree() == pol1.der().degree() + 1

    @given(hyst.polynoms(),
           st.integers(),
           hyst.primes(),
           hyst.polynoms(),
           st.integers()
          )
    def test_modulo(self, poly, value, prime, mod_polynom, mod_int):
        assume(not mod_polynom.trivial())
        # With an interger as argument
        assert poly(value) % prime == ((poly % prime)(value)) % prime
        # With a polynom as argument
        assert (poly + mod_int * mod_polynom) % mod_polynom == poly % mod_polynom
        # With an integer and a polynom as argument
        if (poly % mod_int).trivial():
            assert ((poly % mod_int) % mod_polynom).trivial()
            assert (poly % f.Field(mod_int, mod_polynom)).trivial()

    @given(hyst.polynoms(min_degree=1, max_degree=1),
           hyst.primes(),
    )
    def test_affin_linear_root_in_finite_field(self, polynom, prime):
        assume(not (polynom % prime).constant())
        root = polynom.affin_linear_root(prime)
        assert polynom(root) % prime == 0

    @given(hyst.polynoms(min_degree=1, max_degree=1),
    )
    def test_affin_linear_root(self, polynom):
        # TODO test for constant polynoms (they should raise value errors)
        assume(not polynom.constant())
        root = polynom.affin_linear_root()
        if polynom[0] % polynom[1] == 0:
            assert root is not None
            assert polynom(root) == 0
        else:
            assert root is None

    @given(hyst.polynoms(),
           hyst.primes(),
          )
    def test_roots_are_roots(self, polynom, prime):
        # This test takes very long for big primes
        assume(prime < 50)
        roots = polynom.roots(prime)
        for root in roots:
            assert 0 <= root < prime
            assert polynom(root) % prime == 0

    @given(st.lists(elements = st.integers(),
                    max_size = 20,
                   ),
           hyst.primes(),
    )
    @example([0,0],
             5
    )
    def test_all_roots_are_found(self, roots, prime):
        assume(prime < 4079)
        prime = 11
        p = pol.Polynom(roots=roots)
        found_roots = set(p.roots(prime))
        assert found_roots == set([r % prime for r in roots])

    def test_all_roots_are_found_special_case(self):
        prime = 5
        roots = [0,0]
        p = pol.Polynom(roots=roots)
        found_roots = set(p.roots(prime))
        assert found_roots == set([r % prime for r in roots])

    @given(st.integers(),
           st.integers(),
          )
    def test_extended_euclidian_algorythm(self, n, m):
        assume(n != 0)
        assume(m != 0)
        g, n_factor, m_factor = pol.extended_gcd(n,m)
        assert n % g == 0 and m % g == 0
        assert n * n_factor + m * m_factor == g

    @given(st.integers(),
           hyst.primes()
          )
    def test_inverse_in_finite_field_ints(self, x, p):
        assume(x % p != 0)
        inv = pol.inverse_in_finite_field(x, p)
        assert (x * inv) % p == 1
        assert 0 < inv < p

    @given(hyst.polynoms(trivial_allowed=False),
           hyst.fields,
          )
    def test_inverse_in_finite_field_polynoms(self, poly, field):
        assume(not (poly % field).trivial())
        inv = pol.inverse_in_finite_field(poly, field)
        note("Field: {}, poly: {}, inverse: {}".format(field, poly, inv))
        assert (poly * inv) % field == pol.Polynom(0)
        assert (inv % field == inv)

    @given(hyst.fields,
           hyst.polynoms()
    )
    def test_square_root_in_finite_field(self, field, poly):
        assume(not (poly % field).trivial())
        square = pow(poly, 2, field)
        root1 = square.square_root_in_finite_field(field, "tonelli_shanks")
        root2 = square.square_root_in_finite_field(field, "brute_force")
        assert pow(root1, 2, field) == square
        assert pow(root2, 2, field) == square

    @given(hyst.polynoms())
    def test_square_root_in_Z(self, poly):
        square = poly**2
        note("Test finding the square root of ({})^2 = {}.".format(poly, square))
        root = square.square_root_in_Z()
        assert root == abs(poly)

        # TODO Move this tests to a place they belong


    @given(hyst.polynoms(trivial_allowed=False),
           hyst.fields,
          )
    def test_legendre_symbol_positive(self, poly, field):
        square = (poly * poly) % field
        assume(not square.trivial())
        assert co.legendre_symbol(square, field)


