import logging
import common as co
import numpy as np

def smoothies_to_matrix(smoothies, r_factor_base, a_factor_base, character_base, m, poly):
    """Contruct a binary matrix out of the smoothies

    Each smoothie gets a column in the matrix. Each of this columns is
    made of 4 parts:
    - singum_part
    - rational_factorbase_part
    - algebraic_factorbase_part
    - characterbase_part

    The exact procedure is written down very explicit in Briggs 5.6
    """
    log = logging.getLogger("build_matrix_logger")
    gnfs_matrix = []

    for smoothie in smoothies:
        (a,b) = smoothie
        r_smooth = a + b*m
        # a_smooth = a + b \theta
        r_factor_dict = co.is_smooth(r_smooth, r_factor_base)
        log.info("Calculating the column for the smoothie {}".format(smoothie))

        # signum_part
        signum_part = [r_smooth < 0]
        log.debug("signum_part: {}".format(signum_part))

        # rational_factorbase_part
        rational_factorbase_part = [r_factor_dict[prime] % 2 == 1
                                    for prime in r_factor_base]
        log.debug("rational_factorbase_part: {}".format(
                   rational_factorbase_part))

        # algebraic_factorbase_part
        algebraic_factordict = co.is_smooth((a, b), a_factor_base, m, poly)
        # assert (a,b) was really algebraic smooth
        assert bool(algebraic_factordict)
        algebraic_factorbase_part = [ algebraic_factordict[(r,p)] % 2 == 1
                                      for (r,p) in a_factor_base]
        log.debug("algebraic_factorbase_part: {}".format(
                   algebraic_factorbase_part))

        # characterbase_part
        characterbase_part = [ co.legendre_symbol(a+b*s, q) == -1
                               for (s,q) in character_base]
        log.debug("characterbase_part: {}".format(
                  characterbase_part))

        column = (signum_part +
                 rational_factorbase_part +
                 algebraic_factorbase_part +
                 characterbase_part)
        gnfs_matrix.append(column)
    return np.matrix(gnfs_matrix).transpose()
